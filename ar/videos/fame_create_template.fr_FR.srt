﻿1
00:00:00,000 --> 00:00:04,264
Dans cette courte vidéo, nous allons vous montrer comment créer un modèle de rapport

2
00:00:04,264 --> 00:00:06,686
dans le Gestionnaire de publicités Facebook pour Excel

3
00:00:06,686 --> 00:00:09,767
pour recueillir les données de performances depuis vos comptes publicitaires.

4
00:00:11,267 --> 00:00:15,172
Pour créer un modèle de rapport,
cliquez sur « Gérer les modèles ».

5
00:00:16,307 --> 00:00:17,965
Cliquez sur « Créer un modèle »,

6
00:00:17,965 --> 00:00:21,280
puis personnalisez l’apparence de votre rapport

7
00:00:21,280 --> 00:00:24,051
dans le Gestionnaire de publicités Facebook pour Excel.

8
00:00:24,051 --> 00:00:26,795
Pensez aux indicateurs qui vous intéressent le plus

9
00:00:26,795 --> 00:00:30,376
pour adapter la structuration de votre modèle.

10
00:00:31,922 --> 00:00:33,855
Cliquez sur « Créer ».

11
00:00:33,855 --> 00:00:35,862
Après avoir créé le modèle de rapport,

12
00:00:35,862 --> 00:00:38,533
vous pouvez télécharger les rapports depuis vos comptes publicitaires.

13
00:00:40,417 --> 00:00:43,545
Pour en savoir plus sur le Gestionnaire de publicités Facebook pour Excel,

14
00:00:43,545 --> 00:00:45,338
consultez nos pages d’aide.


