﻿1
00:00:00,000 --> 00:00:04,264
In diesem kurzen Video zeigen wir dir, wie du eine Berichtsvorlage

2
00:00:04,264 --> 00:00:06,686
im Facebook-Werbeanzeigenmanager für Excel erstellst.

3
00:00:06,686 --> 00:00:09,767
Damit hast du die Performance-Daten deiner Werbekonten im Blick.

4
00:00:11,267 --> 00:00:15,172
Klicke zum Erstellen einer Berichtsvorlage
auf „Manage Report Templates“.

5
00:00:16,307 --> 00:00:17,965
Klicke auf „Create New Template“

6
00:00:17,965 --> 00:00:21,280
und passe das Aussehen deines Berichts

7
00:00:21,280 --> 00:00:24,051
im Facebook-Werbeanzeigenmanager für Excel an.

8
00:00:24,051 --> 00:00:26,795
Berücksichtige die für dich besonders relevanten Kennzahlen

9
00:00:26,795 --> 00:00:30,376
und erstelle deine entsprechende Berichtsvorlage.

10
00:00:31,922 --> 00:00:33,855
Klicke auf „Erstellen“.

11
00:00:33,855 --> 00:00:35,862
Nachdem du deine Berichtsvorlage erstellt hast,

12
00:00:35,862 --> 00:00:38,533
kannst du Berichte aus deinen Werbekonten herunterladen.

13
00:00:40,417 --> 00:00:43,545
Mehr über den Facebook-Werbeanzeigenmanager für Excel

14
00:00:43,545 --> 00:00:45,338
erfährst du in unserem Hilfebereich.


